/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const npath = require('path')
const { createFilePath } = require('gatsby-source-filesystem')

exports.onCreateWebpackConfig = ({ stage, actions }) => {
  if (stage === `develop`) {
    actions.setWebpackConfig({
      devtool: 'source-map',
    })
  }
}

// Implement the Gatsby API “createPages”. This is called once the
// data layer is bootstrapped to let plugins create pages from data.
exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const staticPage = npath.resolve(`src/pages/static.js`)
    resolve(
      graphql(
        `
          {
            allMarkdownRemark(
              filter: { frontmatter: { type: { eq: "static" } } }
              limit: 10
            ) {
              edges {
                node {
                  frontmatter {
                    slug
                    type
                  }
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          reject(result.errors)
        }

        // Create pages for each markdown file.
        result.data.allMarkdownRemark.edges.forEach(({ node }) => {
          const slug = node.frontmatter.slug
          createPage({
            path: slug,
            component: staticPage,
            // If you have a layout component at src/layouts/blog-layout.js
            layout: `index`,
            // In your blog post template's graphql query, you can use path
            // as a GraphQL variable to query for data from the markdown file.
            context: {
              slug,
            },
          })
        })
      })
    )
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.fileAbsolutePath && node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
