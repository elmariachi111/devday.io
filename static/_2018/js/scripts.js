/*
 Created on : Jul 4, 2017, 12:43:10 AM
 Author     : Atta-Ur-Rehman Shah (http://attacomsian.com)
 */
$(function () {
    
    new WOW().init();
    var $body = $('body');
    var $navbar = $('.navbar');
    var $navbarCollapse = $('#navbarCollapse');

    var scrolled = function(scrTop) {
        $body.toggleClass('scrolled', scrTop > 100)
    }

    scrolled($body.scrollTop()); //scrollOnLoad

    $(window).scroll(function(evt) {
        scrolled($(this).scrollTop());
    });

    $navbarCollapse.on('show.bs.collapse', function() {
        $navbar.addClass('open')
    })
    $navbarCollapse.on('hide.bs.collapse', function() {
        $navbar.removeClass('open')
    })

    $('a.page-scroll').on('click', function (event) {
        event.preventDefault();
        $navbarCollapse.collapse('hide');

        var top = $($(this).attr('href')).offset().top - 50;
        $('html, body').stop().animate({
            scrollTop: top
        }, 1500, 'easeInOutExpo');
        return false;
    });



/*init();    
    //page scroll
    

    var init = function() {
        var secondFeature = $('#about').offset().top;
        var scroll = $(window).scrollTop();
        if (scroll >= 150) {
            $('.sticky-navigation').css({"background-color": 'rgba(0, 0, 255, 0.85)'});
        } else {
            $('.sticky-navigation').css({"background-color": 'transparent'});
        }
        if (scroll >= secondFeature - 200) {
            $(".mobileScreen").css({'background-position': 'center top'});
        }
    
        return false;
    }*/
    
});
