const activeEnv = process.env.ACTIVE_ENV || process.env.NODE_ENV || 'development'
console.log(`active env: ${activeEnv}`)

require('dotenv').config({
  path: `.env.${activeEnv}`,
})

module.exports = {
  siteMetadata: {
    title: `DEVDAY: Decoding the Future | spring 2021 Faro`,
    description: `DEVDAY is a deep tech festival for tech enthusiasts,
                  developers and people inventing the future. Join us in 
                  Faro (Portugal) in spring 2021 for two days of 
                  talks, workshops, discussions, communities and inspiration.
                  `,
    ogdescription: `DEVDAY is a deep tech festival for developers and
    tech enthusiasts. Join us in spring 2021 in Faro/Portugal for
    talks, workshops and discussions`,
    keywords: `developers,javascript,continuous integration,containerization,design,machine learning,augmented reality,Java,Golang,Rust,PHP,databases`

  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass',
    'gatsby-transformer-remark',
    'gatsby-transformer-json',
    `gatsby-plugin-catch-links`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: false,
        stripMetadata: true,
        defaultQuality: 75,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data/`,
      },
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.GATSBY_CONTENTFUL_SPACE_ID,
        accessToken: process.env.GATSBY_CONTENTFUL_ACCESS_TOKEN,
        downloadLocal: true
      },
    },
    {
      resolve: 'gatsby-source-cloudinary',
      options: {
        api_key: process.env.GATSBY_CLOUDINARY_API_KEY,
        api_secret: process.env.GATSBY_CLOUDINARY_API_SECRET,
        cloud_name: process.env.GATSBY_CLOUDINARY_CLOUD_NAME,
      },
    },
  ],
}
