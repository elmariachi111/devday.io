---
title: InspireMe
path: '/2020/inspire-me'
slug: inspire-me
---

At DEVDAY we want to celebrate software crafts*wo*manship and don't focus on a particular technology or subject. You're free to submit any topic you can think of as a talk proposal.

We're setting up two stages to present 5 talks each. One will focus on the more hard core developer side of "technology", the other on more general technology aspects, e.g. design, agile or programming philosophy. 

DEVDAY's audience consists of developers of all trades and experience levels. We've seen many people of local and national communities and welcomed lots of international participants as well. Be sure to check out the recap websites of [2018](/2018)/[2019](/2019) to get a feeling of what people might expect from talks given at DEVDAY.