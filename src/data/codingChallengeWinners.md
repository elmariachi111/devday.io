---
title: Coding Challenge
path: '/coding-challenge'
slug: coding-challenge-winners
---

## Why should I participate?

Glad you asked! We're actually trying to build something greater
with that repository. While all the code you produce will always
belong to you, we want to glue it together to a runnable and
helpful website developers all over planet earth can use. It's
going to become one of the cornerstones of the **coding earth**
movement, a non profit association founded in Berlin that
organizes e.g. the
[coding berlin meetups](https://www.meetup.com/CODING-BERLIN/).

Besides collaboratively building something together, learning
about PaaS platforms and contributing to the community, the 10
finalizers with the most productive code will take home a unique
present that actually is alive and will enhance your work life balance.
