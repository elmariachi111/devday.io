---
title: Coding Challenge
path: '/coding-challenge'
slug: coding-challenge
---

### All Day: Coding Challenge

This year we're proud to have **[platform.sh](https://platform.sh)** aboard who make our **Coding Challenge** possible. To
make it really challenging, we're offering you 5 preconfigured
notebooks with preinstalled platform.sh tools, IDEs and activated evaluation accounts.

The idea is to collaborate on a **microservice oriented aggregation platform** for news,
events, repos, profiles, [xkcds](https://www.xkcd.com/) or anything coding related piece of information you can think of. You're invited to hack along no matter your grade of experience. A platform.sh evangelist will be around all time to get you onboarded and help you with any questions.
