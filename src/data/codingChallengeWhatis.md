---
title: Coding Challenge
path: '/coding-challenge'
slug: coding-challenge-whatis
---

##### What is platform.sh?

Platform.sh offers a unique **PaaS solution** and allows you to run a
set of applications and their resource dependencies (like databases or caches) as an
**isolated environment**. They allow you to test and q/a your
development progress by sharing URLs specific to each environment.

All changes, be it the addition or upgrades of database resources
or the complete rewrite of dedicated microservices are isolated
between environments so they cannot affect each other. While you
absolutely can host monolithic PHP apps on platform.sh it
especially stands out when building decoupled service
architectures.
