---
title: Privacy Policy
slug: '/privacy-policy'
type: 'static'
---

# Privacy Policy

In the following we (the Turbine Kreuzberg GmbH) will inform you about how personal data is processed when you are using this website.

Personal data is any information relating to an identified or identifiable natural person (“data subject”). Especially information through which you can be identified, directly or indirectly, in particular by reference to an identifier such as a name, telephone number, e-mail address or address. Statistic data that is collected during your visit on our website that cannot be traced to you as an individual person is not regarded as personal data.

You have the opportunity to print out or save this privacy policy by with your browser. Furthermore you can download this Privacy Policy as an PDF by [clicking here](https://turbinekreuzberg.com/content/datenschutzerklaerung/privacy-policy-turbinekreuzberg.com.pdf 'PDF').

### 1. Contact

Your contact is the so-called controller. The controller is responsible for the processing of your personal data in accordance to the EU General Data Protection Regulation. The controller of this website is

Turbine Kreuzberg GmbH  
Ohlauer Straße 43  
10999 Berlin

Tel.: +49 30 28 47 26 40 25  
Fax: +49 30 28 47 26 40 75  
E-Mail: datenschutz@turbinekreuzberg.com

Feel free to contact our data protection officer (DPO) about any question related to data protection in connection with the usage of this website or our third-party service providers. You can reach the DPO via mail or e-mail to the address provided above.

### 2. Processing of personal data on our website

The usage of our website requires the collection of data that is provided automatically by your browser. Particularly the

- IP-adress of the requesting device
- Date and time of the request
- Address of the requsted and requesting website
- Information about the browser and the operating system of the user's device
- Online Identification (for example equipment identity, Session-ID)

Processing this data is necessary to visit the website and to provide the permanent functionality and security of our system. For this reason access data is saved in internal logfiles. Through these we collect statistic data to develop our website in accordance to the ever-changing requirements (for instance the increase of our website usage through mobile devices) and generally for the maintenance and administration of our website. The legal basis for this is Article 6 (1) sentence 1, letter b of the GDPR.

The information saved in the logfiles cannot be traced back to you as an individual – especially your IP-address is saved truncated. The logfiles are saved for 7 days and after that they are anonymized furtherly and archived.

### 3. Passing on data

Data which we have collected are passed on only if:

- You have given an express declaration of consent for this, pursuant to Art. 6, Paragraph 1, Clause 1, Point (a) of the GDPR,

- Further transmission is necessary, pursuant to Art. 6, Paragraph 1, Clause 1, Point (f) of the GDPR, for bringing, exercising or defending legal claims, and no reason exists to suppose that you have a predominant and properly protected interest in preventing your data from being passed on,

- We have a legal duty to pass on your data pursuant to Art. 6, Paragraph 1, Clause 1, Point (c) of the GDPR, or
- This is legally permissible and requisite, pursuant to Art. 6, Paragraph 1, Clause 1, Point (b) of the GDPR, for the handling of contracts with yourself or for the execution of precontractual actions which are being carried out at your request.

A part of the data processing can be handled via service providers. Along with the service providers stated in this Data Protection Policy, these may include in particular computer centres which store our website and databases, IT service providers which maintain our systems, and consultancy firms. Should we pass data on to our service providers, these data may only be used for performance of their tasks. We select and commission these service providers carefully. They are bound contractually to follow our instructions, have suitable technical and organisational measures for the protection of the rights of data subjects, and are monitored by ourselves on a regular basis. 
Further transmission may also be made in connection with requests by government authorities, decisions of the courts and legal proceedings if it is necessary for prosecution or execution at law.

### 4. Duration of storage of personal data

We store personal data only for as long as is necessary to fulfil contractual or statutory duties for which the data were collected. We then erase the data immediately, unless we still need these data until expiry of the statutory period of limitation for purposes of evidence in civil claims or due to statutory duties of storage.

For purposes of evidence we must still store contact data for three years from the end of the year in which business relations with you end. Any claims will expire, under the normal statutory period of limitation, no earlier than at this time.

Thereafter we must also store some of your data for purposes of book-keeping. We have an obligation to do so under statutory duties of documentation which may arise under the German Commercial Code, the German Tax Code, the German Credit and Loans Act, the German Money Laundering Act, and the German Securities Act. The periods stipulated there for storage of documents are two to ten years.

### 5. Your Rights

You have the right at any time to require us to provide information about the processing of your personal data (right of access). When providing you with this information we shall explain the data processing and supply an overview of the data relating to your person which are stored. Should data stored with us be inaccurate or no longer up-to-date, you enjoy the right to have these data corrected (right to rectification). You can also require the erasure of your data (right to erasure or right to be forgotten). Should the erasure exceptionally not be possible due to other legal regulations, the data processing will be restricted, so that in future they are only available for this statutory purpose. You can also have the processing of your data restricted, i.e. if you believe that the data which we have saved are not correct (right to restriction of processing). You also have the right of data portability, i.e. that we send you on request a digital copy of the personal data which you have provided (right to data portability).

To exercise your rights as set out here, you can communicate with the foregoing contact details at any time. This also applies should you wish to receive copies of guarantees for certification of an adequate data-protection level.

You also have the right to object to the data processing based on Art. 6, para., lit. e or f of the GDPR. Finally, you have the right to complain to the regulatory authority to which we are subject. You can exercise this right at a regulatory authority in the member country of your place of residence, of your workplace, or of the place of alleged breach. In Berlin where Turbine Kreuzberg is located the competent regulatory authority is: Data Protection and Freedom of Information Officer, Friedrichstrasse 219, 10969 Berlin.

### 6. Right to withdraw or object

Under Article 7, para. 3 of the GDPR you have the right at any time to withdraw to us any consent which has once been given. This will have as a consequence that in future we no longer continue the data processing based on this consent. The withdrawal of consent shall not affect the lawfulness of processing based on consent before its withdrawal.

Insofar as we process your data on the basis of legitimate interests under Art. 6, para. 1, lit. f GDPR, you have the right under Art. 21 GDPR to object to the processing of your data and to mention grounds relating to your particular situation that in your opinion speak in favour of prevailing legitimate interests. Where personal data are processed for direct marketing purposes, you have a general right of objection which will also be implemented by us without your stating reasons.

If you wish to make use of your right to withdraw or object, a notification without set form to the contact details above will be sufficient.

### 7. Data security

We maintain up-to-date technical measures to ensure data security, particularly for the protection of your personal data against dangers during data transmissions and against cognizance by third parties. These are amended in each case to reflect the current state of technology. To secure the personal data which you have stated on our website, we use transport layer security (TLS), which encrypts the information which you have entered.

### 8. Photography, audio and video recording

You agree to permit us, or any third party licensed by us, to use, distribute, broadcast, or otherwise globally disseminate your likeness, name, voice and words in perpetuity in television, radio, film, newspapers, magazines and other media now available and hereafter developed, both before, during and any time after the Conference, and in any form, without any further approval from you or any payment to you. This grant includes, but is not limited to, the right to edit the media, the right to use the media (alone or together with other information), and the right to allow others to use or distribute the media.

### 9. Amendments to Data Protection Policy

We occasionally update this Data Protection Policy, for instance when we revise our website or statutory or official regulations change.

Date: March 2019
Eine deutsche Version unserer Datenschutzerklärung [finden Sie hier](http://www.turbinekreuzberg.com/de/datenschutzerklaerung).
