---
title: Imprint
slug: '/imprint'
type: 'static'
---

# Impressum

**Turbine Kreuzberg GmbH**  
Ohlauer Straße 43  
10999 Berlin

+49 30 2847 2640 0  
hello@turbinekreuzberg.com

Vertretungsberechtigte Geschäftsführer  
Peter Breuer, Alexander Janthur, Christopher Möhle, Daniel Nill  
Registergericht: Amtsgericht Charlottenburg  
Registernummer: HRB 117511 B

Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz: DE 263548073

Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV: Alexander Janthur, Ohlauer Straße 43, 10999 Berlin
