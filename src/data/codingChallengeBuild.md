---
title: Coding Challenge
path: '/coding-challenge'
slug: coding-challenge-build
---

##### What are we supposed to build?

What are your daily destinations to learn about "developer"
news? DEV DAY and platform.sh like to challenge you to combine
forces to build a website that displays lots of developer
related stuff, be it meetups in your area, news around the
globe or conferences you might want to attend.

We've setup a
very basic website _scaffold_ on platform.sh that you're
supposed to fill with life during the coding challenge. Start
by visiting our [github repo](https://github.com/coding-berlin/devday-codingchallenge) and read its [README file](https://github.com/coding-berlin/devday-codingchallenge/blob/master/README.md).
