---
title: Coding Challenge
path: '/coding-challenge'
slug: coding-challenge-setup
---

##### How are things playing together?

[Our monorepo](https://github.com/coding-berlin/devday-codingchallenge)
currently consists of 4 basic applications.
They can be spun up locally using docker-compose
or forked and run on a new platform.sh environment.

- _scaffold_ is a React application that combines all
  data by the others to one nice web frontend.
- _coordinator_ is a glue service that knows about
  all applications by either querying the platform
  environment or being (locally) configured accordingly. It
  yields http endpoints to _scaffold_ which expose
  all other services in the application.
- _rssreader_ is a massively simplified RSS reader,
  built on zeit/micro that on day 0 requests a remote RSS
  feed and passes it through.
- _calendarservice_ is the simplest possible Symfony
  setup that currently only responds with a string. It can
  be used as a demo of how to implement PHP based apps but
  also be extended to deliver real calendar events
