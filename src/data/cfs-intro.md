---
title: Call for Speakers intro
path: '/cfp-intro'
slug: cfs-intro
---

### What it feels like

DEVDAY is one of Portugal's largest developer events. In 2019 we attracted more than 250 attendees and in 2021 we will double that. It's your best chance to reach a highly interested technical audience and make your mark. 

You can either apply to give a talk at DEVDAY'21 or host your own workshop session (~4-5hs) the day before. Show everyone your take on »decoding the future«. Or get your audience's hands dirty by doing a practice lesson. Topic-wise there are little to no restrictions, but we're especially looking for talks that could be interesting for a broader developer audience.



