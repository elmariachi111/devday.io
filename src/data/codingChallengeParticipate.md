---
title: Coding Challenge
path: '/coding-challenge'
slug: coding-challenge-participate
---

##### How can I participate?

- Ask the local DEV DAY team to get access to the platform.sh
  repository and clone it. You can also [fork our github monorepo](https://github.com/coding-berlin/devday-codingchallenge) and add branches that deploy automatically as environment.
- To run the code locally (which is not
  mandatory but much more productive), you might want to use the
  accompanying docker-compose file to setup running environments for all the needed microservices.
- Choose a challenge that feels like a fit to your personal skillset and fork a personal environment for it.
- Create a new microservice and implement your challenge.
- Add some frontend component for it in
  the scaffold. Ask a local frontend guy for her support if needed
  or simply create a JSON output so someone else can pick it up and
  improve your environment.
- When everything looks nice, create a PR and ask to have it merged into master.

Congrats: you're hereby qualified for the final coding challenge awards :)
