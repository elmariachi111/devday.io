---
title: Dev Day 2018
path: "/about"
---

## This was Dev Day 2018

A wide range of topics, an exclusive choice of speakers, a day to remember for developers: that was Dev Day 2018. We heard 11 amazing talks, e.g. on Kubernetes setups, website and application optimization, command line tooling or the Elixir programming language. Finally the audience awarded the best speakers with cheerful applause and they got away with unique trophies. 

Dev Day is a yearly developer conference organized by [Turbine Kreuzberg](https://turbinekreuzberg.com/) that happens simultaneously in Faro / Portugal and Berlin. All talks are live streamed during the event so everyone can join the other location's talks, too. And there's certainly more to that: In 2018 more than 40 developers joined the coding challenge that we've set up on HackerRank, many people followed the panel discussions during lunch time and the networking throughput was extremely high.

If you want to be part of the next Dev Day (it's going to happen on *May, 25th 2019* so save the date!), make sure to leave your email address [in the form below](#section-newsletter). We'll drop you an email as soon as our call for papers and the early bird ticket stages are starting.
