import React from 'react'
import Wildcard from './Wildcard'
import { Fade, Zoom } from 'react-reveal'
import { Link, StaticQuery, graphql } from 'gatsby'

import { Container, Row, Col } from 'reactstrap'
//import platformsh from '../media/sponsors/platformsh.png'
import platformsh from '../media/sponsors/platformsh_white.svg'

//const ccImage =
//  'https://res.cloudinary.com/turbinekreuzberg/image/upload/c_crop,g_center,h_800,w_1920,x_0,y_199/v1529574086/devday18/berlin/IMG_2973.jpg'

import cometPlanets from '../media/sponsors/comet-planets.svg'

const query = graphql`
  {
    markdownRemark(frontmatter: { slug: { eq: "coding-challenge" } }) {
      html
    }
  }
`

const CCContent = () => (
  <StaticQuery
    query={query}
    render={data => (
      <Row>
        <Col>
          <Fade left distance="20px">
            <div
              dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }}
            />
          </Fade>
        </Col>
        <Col
          sm={4}
          className=" d-flex  align-items-center justify-content-center"
        >
          <div className="pl-4 flex-fill">
            <Fade right distance="20px">
              powered by
              <a href="https://platform.sh" target="_blank" className="">
                <img src={platformsh} />
              </a>
            </Fade>
            <Zoom>
              <Link to="/coding-challenge" className="btn btn--primary mt-5">
                Learn more
              </Link>
            </Zoom>
          </div>
        </Col>
      </Row>
    )}
  />
)

export default props => (
  <Wildcard
    color="secondary"
    revealClasses="margin-top"
    backgroundClass="bg-image"
    align="right"
    backgroundStyle={{
      backgroundImage: `url(${cometPlanets})`,
      backgroundPosition: 'cover',
    }}
    content={<CCContent />}
  />
)
