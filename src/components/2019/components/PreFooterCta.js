import React from 'react'
import logosvg from '../media/logo_devday.svg'
import { Container, Row, Col } from 'reactstrap'
import Link from 'gatsby-link'

export default class PreFooterCta extends React.Component {
  render() {
    return (
      <section id="cta">
        <div className="cta cta--darken cta--primary" data-offset="90%">
          <div className="cta__copy text-center">
            <div className="d-inline-block margin-top margin-bottom">
              <a className="btn btn--secondary" href="#">
                Get your tickets today
              </a>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
