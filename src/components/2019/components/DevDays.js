import React from 'react'

import Section from '../../common/Section'
import { Container, Row, Col } from 'reactstrap'

export default props => (
  <Section>
    <h4>Not the Dev Day you've been looking for?</h4>
    <p>
      We've got you covered. Here are other »Dev Day« events in 2020 from all over the world. We're not affiliated with them in any way, but we'd really like to support each other.
    </p>
    <Row>
      <Col>
        <strong>
          <a href="https://www.devday.lk/" target="_blank">
            devday.lk
          </a>
        </strong>
        <br />
        Sri Lanka
      </Col>
      <Col>
        <strong>
          <a href="https://aws.amazon.com/pt/events/devday/" target="_blank">
            AWS DevDay
          </a>
        </strong>
        <br />
      </Col>
      <Col>
        <strong>
          <a href="https://www.devday.be/" target="_blank">
            devday.be
          </a>
        </strong>
        <br />
        Belgium
      </Col>
      <Col>
        <strong>
          <a href="https://devday.devisland.com/" target="_blank">
            devday.devisland.com
          </a>
        </strong>
        <br />
        Brazil
      </Col>
      <Col>
        <strong>
          <a href="https://devdays.lt/" target="_blank">
            devdays.lt
          </a>
        </strong>
        <br />
        Vilnius <br /> May 5-7th
      </Col>
      <Col>
        <strong>
          <a href="https://www.devday.de/" target="_blank">
            devday.de
          </a>
        </strong>
        <br />
        Dresden  <br /> May 5th
      </Col>
      <Col>
        <strong>
          <a href="https://www.devdays.com/" target="_blank">
            devdays.com (FHIR)
          </a>
        </strong>
        <br />
        Cleveland OH (USA)  <br /> June 16-18th
      </Col>
    </Row>

  </Section>
)
