import { default as React, useState } from 'react'
import { Flip } from 'react-reveal'
import { Col, Row } from 'reactstrap'
import Wildcard from '../common/Wildcard'
import SpeakerSocialElements from '../common/Talks/SpeakerSocialElements'
import bg4 from './media/bg4.svg'
import Img from 'gatsby-image'
import RichText from '../common/RichText'
import { useStaticQuery, graphql, Link } from 'gatsby'
import { SpeakerImage } from './Contributions'
   
export const ContributionSummary = (props) => {
    const data = useStaticQuery(graphql`
    fragment speakerFields on ContentfulSpeaker {
        id
        name
        position
        company
        twitterHandle
        githubHandle
        linkedInProfile
        claim
        image {
          title
          localFile {
            childImageSharp {
              fluid(maxWidth: 500) {
                ...GatsbyImageSharpFluid_tracedSVG
              }
            }
          }
        }
        bio {
          childMarkdownRemark {
            html
          }
        }
      }
      
      
      query {
          talks: allContentfulTalk(
            filter: { year: { eq: 2020 }, node_locale: { eq: "de" } }
            sort: { fields: [speaker___name], order: ASC }
          ) {
            edges {
              node {
                id
                node_locale
                title
                slotTime
                location
                youtubeUrl
                slides
                winner
                highlights {
                  highlights
                }
                statement {
                  statement
                }
                abstract {
                  childMarkdownRemark {
                    html
                  }
                }
                speaker {
                  ...speakerFields
                }
              }
            }
          }
      
        workshops: allContentfulWorkshop(
          filter: { year: { eq: 2020 }, node_locale: { eq: "de" } }
          sort: { fields: [title], order: ASC }
        ) {
          edges {
            node {
              id
              node_locale
              title
              companyName
              companyLogo {
                file {
                  url
                }
              }
              speaker {
                 ...speakerFields
              }
            }
          }
        }
      }
        
    `)

    const talks = data.talks.edges.map(({ node }) => node)
    const workshops = data.workshops.edges.map(({ node }) => node)
    return (
        <div>
          <Link to="/talks"><h2>Speakers</h2></Link>
          <Row className="mt-5">
          {talks.map( (talk, idx) => {
              return talk.speaker.map(s => (
                  <Col sm={6} lg={3} className="text-center mb-5">
                  <SpeakerImage image={s.image} />
                  <h3 className="margin-none">{s.name}</h3>
                  <p className="sub-head-1 mb-0">{s.position}</p>
                  
                  <p className="sub-head-2 mt-0">{s.position} | {s.company}</p>
                
                  <strong>{talk.title}</strong>
                  </Col>
              ))
          })}
          </Row>

          <Link to="/workshops"><h2>Workshops</h2></Link>
          <Row>
          {workshops.map( (workshop, idx) => (
            <Col sm={6} className="text-center mb-5">
                <h3>{workshop.title}</h3>
              <Row>
              {workshop.speaker.map(s => (
                <Col>
                <SpeakerImage image={s.image} />
                <h4 className="margin-none">{s.name}</h4>
                <p className="sub-head-1 mb-0">{s.position}</p>
                <p className="sub-head-2 mt-0">{s.position} | {s.company}</p>                
                </Col>
              ))}
              </Row>
            </Col>
          ))}
          </Row>
        </div>
    )
}