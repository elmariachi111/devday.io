import React from 'react'
import {
  Hero,
  HeroBody,
  Container,
  Columns,
  Column,
  Title,
  Field,
  Label,
  Control,
  Icon,
  Input,
  Radio,
  Button,
} from 'bloomer'

export default ({ content }) => (
  <Hero isSize="medium" isColor="dark" id="section-newsletter">
    <HeroBody>
      <a name="section-newsletter" />
      <Container>
        <Columns id="mc_embed_signup">
          <Column isSize="1/2" isOffset="1/4" id="">
            <Title isSize={2}>Remind me about Dev Day 19</Title>
            <Title isSize={4} isPaddingless>
              <Icon
                isSize="large"
                className="fa fa-calendar-alt"
                style={{ marginLeft: '-.75rem' }}
              />
              Sat, May 25th 2019
            </Title>
            <form
              name="devday-newsletter"
              method="POST"
              data-netlify="true"
              netlify-honeypot="last-name"
            >
              <input type="hidden" name="form-name" value="devday-newsletter" />

              <Field>
                <Label>Your Email address</Label>
                <Control hasIcons>
                  <Icon isSize="small" className="fa fa-at" isAlign="left" />
                  <Input
                    placeholder="email address"
                    type="email"
                    name="email"
                    className="required email"
                    autoComplete="email"
                    required
                  />
                </Control>
              </Field>

              <Field required>
                <Label>Your name</Label>
                <Control>
                  <Input
                    placeholder="how should we call you?"
                    type="text"
                    defaultChecked=""
                    name="name"
                    className=""
                    autoComplete="given-name"
                    required
                  />
                </Control>
              </Field>
              <p className="is-hidden">
                {/* this is a h ö n e y p ö t  */}
                <label>
                  What is your last name?
                  <input name="last-name" />
                </label>
              </p>
              <Field className="fld_pad">
                <input
                  className="is-checkradio is-white"
                  type="radio"
                  id="accept_privacy"
                  name="accept_privacy"
                  value="privacy_accepted"
                  required
                />
                <label htmlFor="accept_privacy">
                  I've read and understood the{' '}
                  <a href="/privacy-policy" target="_blank">
                    data privacy rules
                  </a>
                </label>
              </Field>

              <div data-netlify-recaptcha="true" netlify-recaptcha />

              <Field className="fld_pad">
                <Control>
                  <Button
                    type="submit"
                    isSize="large"
                    isColor="info"
                    isFullWidth
                    value="Subscribe"
                    name="subscribe"
                  >
                    Subscribe
                  </Button>
                </Control>
              </Field>
            </form>
          </Column>
        </Columns>
      </Container>
    </HeroBody>
  </Hero>
)
