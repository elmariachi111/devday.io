import React from 'react'
import { animated } from 'react-spring'


const trans = (x, y) => `translate3d(${x}px,${y}px,0) translate3d(-50%,-50%,0)`


export default function Goo({ trail }) {
    return (
        <div className="goo">
            <svg style={{ position: 'absolute', width: 0, height: 0 }}>
                <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="30" />
                    <feColorMatrix in="blur" values="1 0 0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0 30 -7" />
                </filter>
            </svg>
            <div className="hooks-main" >
                {trail.map((props, index) => (
                    <animated.div key={index} style={{ transform: props.xy.interpolate(trans) }} />
                ))}
            </div>
        </div>
    )
}
