import React from 'react'

import Talk from './Talk'
import Schedule from './Schedule'
import classnames from 'classnames'
import { Row, Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Toggle from 'react-bootstrap-toggle'
import { faClock } from '@fortawesome/free-solid-svg-icons'
import Slide from 'react-reveal/Slide'

const Header = ({ tzBerlin, changeTz }) => (
  <Row className="d-none d-sm-flex">
    <Col sm={2} style={{ borderBottom: '3px solid blue' }}>
      <div
        style={{
          position: 'relative',
          left: '-15px',
        }}
      >
        <span className="sub-head-2">timezone</span>
        <Toggle
          onClick={changeTz}
          on={'Berlin'}
          off={'Faro'}
          size="xs"
          onClassName="btn--primary"
          offClassName="btn--secondary"
          width={120}
          height={30}
          active={tzBerlin}
        />
      </div>
    </Col>
    <Col sm={10} style={{ borderBottom: '3px solid blue' }}>
      <Row>
        <Col xs={6} className="text-center">
          <h6>BERLIN</h6>
        </Col>
        <Col xs={6} className="text-center">
          <h6>FARO</h6>
        </Col>
      </Row>
    </Col>
  </Row>
)
const EventBlock = ({ event, timezone, idx }) => (
  <Slide top opposite appear>
    {event.type == 'Schedule' ? (
      <Schedule
        schedule={event}
        timezone={timezone}
        key={`sch-{event}-${idx}`}
      />
    ) : (
      <Talk talk={event} timezone={timezone} key={event.id} />
    )}
  </Slide>
)

const EventRow = ({ timezone, slot }) => (
  <Row>
    {slot.events.both.length > 0 ? (
      <Col xs={12}>
        {slot.events.both.map((event, idx) => (
          <EventBlock
            timezone={timezone}
            event={event}
            idx={idx}
            key={`both-${idx}`}
          />
        ))}
      </Col>
    ) : (
      <>
        <Col xs={12} md={6}>
          {slot.events.berlin.map((event, idx) => (
            <EventBlock
              timezone={timezone}
              event={event}
              idx={idx}
              key={`berlin-${idx}`}
            />
          ))}
        </Col>
        <Col xs={12} md={6}>
          {slot.events.faro.map((event, idx) => (
            <EventBlock
              timezone={timezone}
              event={event}
              idx={idx}
              key={`faro-${idx}`}
            />
          ))}
        </Col>
      </>
    )}
  </Row>
)

export default class Talks extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      tzBerlin: true,
    }
  }

  getTimezone() {
    return this.state.tzBerlin ? 'UTC+2' : 'UTC+1'
  }

  getSlots() {
    const slots = {}
    const tz = this.getTimezone()

    this.props.events.forEach(e => {
      const localTime = e.slotTime.setZone(tz)
      const hour = localTime.hour
      if (!slots[hour]) {
        slots[hour] = { hour, events: { berlin: [], faro: [], both: [] } }
      }
      slots[hour].events[e.location.toLowerCase()].push(e)
    })
    return slots
  }

  render() {
    return (
      <div>
        <Header
          changeTz={() => {
            this.setState({ tzBerlin: !this.state.tzBerlin })
          }}
          tzBerlin={this.state.tzBerlin}
        />
        {Object.values(this.getSlots()).map(slot => {
          return (
            <Row className="clipcard" key={slot.hour}>
              <Col xs={12} md={2} lg={1}>
                <Slide spy={this.state.tzBerlin} left opposite appear>
                  <p className="clipcard__head">{slot.hour}:00</p>
                </Slide>
              </Col>
              <Col xs={12} md={10} lg={11}>
                <EventRow slot={slot} timezone={this.getTimezone()} />
              </Col>
            </Row>
          )
        })}
        <div
          className="d-block d-sm-none"
          style={{
            position: 'fixed',
            right: 110,
            bottom: 10,
            width: 20,
            zIndex: 1030,
          }}
        >
          <span className="sub-head-2">timezone</span>
          <Toggle
            onClick={() => this.setState({ tzBerlin: !this.state.tzBerlin })}
            on={'Berlin'}
            off={'Faro'}
            size="xs"
            onClassName="btn--primary"
            offClassName="btn--secondary"
            width={120}
            height={30}
            active={this.state.tzBerlin}
          />
        </div>
      </div>
    )
  }
}
