import React from 'react'
import {
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap'
import SpeakerSocialElements from './SpeakerSocialElements'

export default class TalkModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
    }
  }

  render() {
    const speaker = this.props.speaker
    const talk = this.props.talk

    return (
      <Modal
        isOpen={this.props.open}
        toggle={this.props.toggle}
        className={this.props.className}
        size="lg"
      >
        <ModalHeader toggle={this.props.toggle}>{talk.title}</ModalHeader>
        <ModalBody>
          <div
            dangerouslySetInnerHTML={{
              __html: talk.abstract.childMarkdownRemark.html,
            }}
          />

          <h6 className="mb-0">{speaker.name}</h6>
          <p className="sub-head mb-0">{speaker.position}</p>
          <p className="sub-head-2 ">{speaker.company}</p>

          <Row>
            <Col md={4}>
              {speaker.image && (
                <div className="text-center">
                  <img
                    src={speaker.image.resolutions.src}
                    className="img-fluid"
                    alt="{speaker.name}"
                  />
                </div>
              )}
            </Col>
            <Col md={8}>
              <blockquote>
                <p>{speaker.claim}</p>
              </blockquote>
              <div
                className="mt-4"
                dangerouslySetInnerHTML={{
                  __html: speaker.bio.childMarkdownRemark.html,
                }}
              />
            </Col>
          </Row>
        </ModalBody>

        <ModalFooter style={{ display: 'block' }}>
          <div className="d-flex justify-content-around align-items-center">
            <SpeakerSocialElements speaker={speaker} iconSize="2x" />
            <div className="">
              <Button color="-secondary" onClick={this.props.toggle}>
                Close
              </Button>
            </div>
          </div>
        </ModalFooter>
      </Modal>
    )
  }
}
