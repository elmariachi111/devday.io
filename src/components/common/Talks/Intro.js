import React from 'react'
import { Link } from 'gatsby'
import ServiceImage from '../ServiceImage'
import { Fade } from 'react-reveal'

const imgs = [
  'https://res.cloudinary.com/turbinekreuzberg/image/upload/t_maxeco/devday18/faro/33682546_2135113943412050_1483181133637091328_o.jpg',
  'https://res.cloudinary.com/turbinekreuzberg/image/upload/a_auto_left.auto_right,c_fill,g_center,h_768,w_1024,x_557,y_527/a_0/v1530484938/devday18/berlin/IMG_2946.jpg',
  'https://res.cloudinary.com/turbinekreuzberg/image/upload/c_scale,q_auto:eco,w_1024/v1529574035/devday18/berlin/20180526_170249.jpg',
  'https://res.cloudinary.com/turbinekreuzberg/image/upload/c_scale,q_auto:eco,w_1024/v1530484962/devday18/faro/IMG_3098.jpg',
]
//const imgs =  'https://res.cloudinary.com/turbinekreuzberg/image/upload/t_maxeco/devday18/faro/33682546_2135113943412050_1483181133637091328_o.jpg'

export default props => (
  <ServiceImage
    wide
    align="left"
    color="primary"
    title="What it feels like"
    image={imgs}
    size={5}
    cta={
      <Link
        to="/talks-2"
        className="sub-head dashed dashed--reverse dashed--hover in-view__child in-view__child--in"
      >
        How are talks working at DEV DAY?
      </Link>
    }
  >
    <Fade left distance="20px">
      <p>
        What's a conference without inspiring talks? For DEV DAY'19 we selected
        skilled speakers who deliver a broad range of topics of all development
        aspects. They'll show us how far they can stretch the term "beyond" by
        getting their hands dirty on doing practice lessons, live coding and
        demos.
      </p>
      <p>
        We're building the conference spaces in Faro and Berlin to be as open as
        possible, so you'll have a good chance to run into the speakers during
        the day and ask them any questions you might have come up with during
        their talks. In the end of the day our audiences in Faro and Berlin will
        vote for the exclusive <strong>Best Speaker Award</strong>, a unique
        trophy that will never let the winners forget that they gave a talk at
        DEV DAY'19.
      </p>
    </Fade>
  </ServiceImage>
)
