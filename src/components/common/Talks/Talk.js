import React from 'react'

import { Row, Col } from 'reactstrap'
import TalkModal from './TalkModal'
import Slide from 'react-reveal/Slide'

/*

<Col xs={12} md={2} lg={1}>
          <Slide spy={this.props.timezone} top opposite appear>
            <p className="clipcard__head">{localTime.toFormat('HH:mm')}</p>
          </Slide>
        </Col>
          */

export default class Talk extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false,
    }
    this.toggleModal = this.toggleModal.bind(this)
  }
  toggleModal() {
    this.setState(prevState => ({
      modal: !prevState.modal,
    }))
  }

  render() {
    const talk = this.props.talk
    const speaker = talk.speaker[0]

    //margin-top
    return (
      <Row className="clipcard ">
        <Col xs={12} md={10} lg={11}>
          <div className="clipcard__body">
            <a className="clipcard__link" onClick={this.toggleModal}>
              <span className="sr-only">See more</span>
            </a>
            {speaker.image && (
              <img
                className="clipcard__thumb"
                src={speaker.image.resolutions.src}
                width="36"
                height="36"
              />
            )}

            {speaker.bio && (
              <TalkModal
                talk={talk}
                speaker={speaker}
                open={this.state.modal}
                toggle={this.toggleModal}
              />
            )}

            <div className="clipcard__info">
              <div className="d-block d-sm-none">{talk.location}</div>
              <span className="clipcard__subtitle ">
                <span className="sub-head-2 ">{speaker.name}</span>
              </span>
            </div>
            <h6 className="clipcard__title">{talk.title}</h6>
          </div>
        </Col>
      </Row>
    )
  }
}
