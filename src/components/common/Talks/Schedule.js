import React from 'react'
import { Row, Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default ({ timezone, schedule }) => (
  <Row className="clipcard ">
    <Col xs={12} md={10} lg={11}>
      <div className="clipcard__body">
        <div className="clipcard__thumb">
          <FontAwesomeIcon icon={schedule.icon} size="2x" />
        </div>
        <div className="clipcard__info">
          <div className="d-block d-sm-none">{schedule.location}</div>
        </div>

        <h6 className="clipcard__title">{schedule.title}</h6>
      </div>
    </Col>
  </Row>
)
