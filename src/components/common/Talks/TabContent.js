export default ({}}) => (
  <div
    className={classnames({
      'tabs__item--active': props.active,
      tabs__item: true,
    })}
    tabIndex="0"
    role="tab"
    aria-selected={props.active ? 'true' : 'false'}
  >
    {props.talks.map((talk, idx) =>
      talk.type == 'Schedule' ? (
        <Schedule
          schedule={talk}
          timezone={props.timezone}
          key={`sch-${props.location}-${idx}`}
        />
      ) : (
        talk.speaker && (
          <Talk talk={talk} timezone={props.timezone} key={talk.id} />
        )
      )
    )}
  </div>
)