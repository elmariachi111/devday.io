import React from 'react'
import { Fade } from 'react-reveal'
export default ({ commits }) => (
  <div>
    {commits.map(c => {
      const commitDate = new Date(c.commit.author.date)
      const firstLine = c.commit.message.split('\n')[0]
      return (
        <div key={c.sha}>
          <Fade top distance="20px">
            <h6>{firstLine}</h6>
            <div className="media">
              <img
                src={c.committer.avatar_url}
                className="align-self-start mr-3"
                alt={c.commit.author.name}
                width={75}
              />
              <div className="media-body">
                <p className="sub-head my-0">{c.commit.author.name}</p>
                <p className="sub-head-2 my-0">
                  {commitDate.toLocaleDateString()} -{' '}
                  {commitDate.toLocaleTimeString()}
                </p>
                <p className="sub-head-2 my-0">
                  <a href={c.html_url}>{c.sha}</a>
                </p>
              </div>
            </div>
          </Fade>
        </div>
      )
    })}
  </div>
)
