import React from 'react'

import { Carousel } from 'react-responsive-carousel'
import "react-responsive-carousel/lib/styles/carousel.min.css";

export default ({ slides }) => (
  <Carousel transitionTime={800}
    showThumbs={false}
    infiniteLoop={true}
    autoPlay={true}
    dynamicHeight={true}

  >
    {slides.map((slide, idx) => (
      <div key={`im-${idx}`}>
        <img src={slide.src} alt={slide.altText} />
        <p className="legend">{slide.caption}</p>
      </div>
    ))}

  </Carousel>
)
