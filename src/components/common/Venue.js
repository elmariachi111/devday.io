import React from 'react'

import { Row, Col } from 'reactstrap'
import { DefaultPlayer as Video } from 'react-html5video'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import VenueMap from './VenueMap'
import { faVideo, faMap } from '@fortawesome/free-solid-svg-icons'

const defaultViewPort = {
  width: '100%',
  height: 400,
  zoom: 16,
}

export default class Venue extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mode: 'map',
    }
  }

  render() {
    return (
      <div className="d-block">
        <Row>
          <Col>
            <h4>{this.props.name}</h4>
          </Col>
        </Row>
        <div className="d-block">
          <VenueMap location={this.props.location} />
          {this.props.address}
        </div>
      </div>
    )
  }
}
