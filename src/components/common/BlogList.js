import React from 'react'
import { Container, Row, Col } from 'reactstrap'

export default props => (
  <div className="blog-list__post in-view__child--in">
    {props.subtitle && (
      <span className="blog-list__info sub-head">{props.subtitle}</span>
    )}
    <h5 className="blog-list__title">{props.title}</h5>
    {props.children && <div className="text-body-3">{props.children}</div>}
    {props.href && (
      <p className="blog-list__link text-xl-right">
        <a
          className="sub-head dashed dashed--reverse--md in-view__child in-view__child--in"
          href={props.href}
        >
          Read the Story
        </a>
      </p>
    )}
  </div>
)
