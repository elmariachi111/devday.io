import React from 'react'

import ReactMapGL, { Marker, NavigationControl } from 'react-map-gl'
import 'mapbox-gl/dist/mapbox-gl.css'
import markerSvg from './media/map-marker.png'

const defaultViewPort = {
  width: '100%',
  height: 400,
  zoom: 16,
}

export default class Venue extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      location: {
        ...defaultViewPort,
        ...props.location,
      },
      client: false,
    }
  }
  componentDidMount() {
    this.setState({
      client: true,
    })
  }
  render() {
    return (
      <div className="map ">
        <div className="google-map">
          {this.state.client ? (
            <ReactMapGL
              {...this.state.location}
              mapboxApiAccessToken={`${
                process.env.GATSBY_REACT_APP_MAPBOX_ACCESS_TOKEN
                }`}
              reuseMaps={true}
              scrollZoom={false}
              onViewportChange={viewport => {
                viewport.width = '100%'
                this.setState({ location: viewport })
              }}
            >
              <div style={{ position: 'absolute', right: 0 }}>
                <NavigationControl
                  onViewportChange={viewport => {
                    console.log(viewport)
                    viewport.width = '100%'
                    this.setState({ location: viewport })
                  }}
                />
              </div>

              <Marker {...this.props.location} offsetLeft={-15} offsetTop={-40}>
                <img src={markerSvg} style={{ height: 40 }} />
              </Marker>
            </ReactMapGL>
          ) : (
              ''
            )}
        </div>
      </div>
    )
  }
}
