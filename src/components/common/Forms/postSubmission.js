import _map from 'lodash.map'

export default (formName, submission) => {
  const encoded = _map(
    {
      'form-name': formName,
      ...submission,
    },
    (val, key) => encodeURIComponent(key) + '=' + encodeURIComponent(val)
  ).join('&')

  if ('development' === process.env.NODE_ENV) {
    console.log(submission)
    console.log(encoded)
    return Promise.resolve(submission)
  }

  return fetch('/', {
    method: 'POST',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    body: encoded,
  })
}
