import React from 'react'
import BlogList from '../BlogList'

import Carousel from '../Carousel'

export default ({ data }) => (
  <div>
    <h3>Inspiration</h3>
    <p>
      If you don't know what you should talk about we made up some arbitrary
      talk topics for you. If you want and can relate to one of them feel free
      to use it. By the way: they are somewhat remotely related to our last
      year's feedback ;)
    </p>
    {/*<Carousel
      slides={data.inspiration.edges.map(edge => edge.node)
      />*/}
  </div>
)
