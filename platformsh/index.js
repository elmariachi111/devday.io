require('dotenv').config()
const { send } = require('micro')
const platformsh = require('platformsh-client')
const cors = require('micro-cors')()
const request = require('request')

const client = new platformsh.default({
  api_token: process.env.PLATFORMSH_API_TOKEN,
})

const CACHE_MAX_AGE = 3 * 60 * 1000
let cached = null
let lastCached = null

const getGithub = () => {
  const token = process.env.GITHUB_API_TOKEN
  return new Promise((resolve, reject) => {
    request(
      {
        url:
          'https://api.github.com/repos/coding-berlin/devday-codingchallenge/commits',
        headers: {
          'User-Agent': 'Dev Day Coding Challenge Backend',
          Authorization: `token ${token}`,
        },
      },
      (error, response, body) => {
        if (error) {
          console.log(body)
          return reject(error)
        }

        const data = JSON.parse(body)
        resolve(data)
      }
    )
  })
}

const getPlatform = () => {
  return new Promise((resolve, reject) => {
    client.getProjects().then(projects => {
      const project = projects[0]

      const response = []

      project.getEnvironments().then(environments => {
        environments.forEach(env => {
          response.push({
            title: env.title,
            name: env.name,
            url: `https://${env.data.edge_hostname}`,
            routes: env.getRouteUrls(),
          })
        })
        resolve(response)
      })
    })
  })
}

const handler = (req, res) => {
  const now = new Date().getTime()
  if (cached && lastCached) {
    if (now - lastCached < CACHE_MAX_AGE) {
      return send(res, 200, cached)
    }
  }
  console.log('getting new content')
  const platformsh = getPlatform()
  const github = getGithub()
  Promise.all([platformsh, github]).then(values => {
    lastCached = now
    cached = {
      platformsh: values[0],
      github: values[1],
    }
    send(res, 200, cached)
  })
}

module.exports = cors(handler)
