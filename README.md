# gatsby-starter-bloomer

A Gatsby starter based on [Bulma](https://bulma.io/) and [Bloomer](https://bloomer.js.org/).

[Demo](https://gatsby-starter-bloomer.netlify.com/)

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/).

## Features

- Based on [gatsby-starter-default](https://github.com/gatsbyjs/gatsby-starter-default)
- Sass preprocessor (with [gatsby-plugin-sass](https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-sass))
- [Bulma CSS Framework](https://bulma.io/) with its [Bloomer react components](https://bloomer.js.org/)
- [Font-Awesome icons](https://fontawesome.com/icons)
- Includes a simple fullscreen hero w/ footer example

## Content Sources and dynamic technologies

- Contentful
- Cloudinary (images)
- Netlify forms

## Deployment

as soon as you push to master, the site is deployed automatically to production. Every branch connected to a merge request will update its review environment automatically.

## Dev environment

create an .env.development file by copying the .env.dist file first and provide your access tokens to it.

Run it by :

```sh
npm run dev
```

or use Gatsby's own tooling:

```sh
gatsby develop
gatsby build # render ssred files / site to ./public
```

## Notes

The version for `gatsby-plugin-sass` [is locked](https://github.com/gatsbyjs/gatsby/issues/4457#issuecomment-371859766) to fix css not deploying in production.

For lightboxes I had to rely on a yet unmerged hotfix that will find its way into `react-image-lightbox` when its released as 5.0.1. That should happen soon (Nov 18)

To build dynamic SVGs, convert the designer's originals to a react component first. Copy their path definitions to an own XML file and parse them to a JSON file using `svg2json.js` 