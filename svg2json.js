var fs = require('fs'),
    xml2js = require('xml2js');

var parser = new xml2js.Parser();
const fn = __dirname + '/src/components/2020/media/ex.xml';

fs.readFile(fn, function (err, data) {

    parser.parseString(data, function (err, result) {
        const paths = result.paths.path.map(p => {
            return {
                d: p.$.d,
                stroke: p.$.stroke
            }
        })
        const jsp = JSON.stringify(paths)
        console.log(jsp)

    });
});